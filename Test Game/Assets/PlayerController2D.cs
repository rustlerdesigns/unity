﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerController2D : MonoBehaviour {

    public float moveSpeed = 0;
    public float jumpHeight = 0;

    public Transform groundpoint;
    public float radius;
    public LayerMask groundMask;
    public LayerMask trampGround;

    bool isGrounded;
    bool isTramp;
    Rigidbody2D rb2d;

	void Start () {
        rb2d = GetComponent<Rigidbody2D>(); ;
	}


    void Update() {

        transform.rotation = Quaternion.Euler(0, 0, 0);
        Vector2 moveDir = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, rb2d.velocity.y);
        rb2d.velocity = moveDir;

        isGrounded = Physics2D.OverlapCircle(groundpoint.position, radius, groundMask);
        isTramp = Physics2D.OverlapCircle(groundpoint.position, radius, trampGround);

        if (Input.GetAxisRaw("Horizontal")  == 1)
            transform.localScale = new Vector3(1, 1, 1);
        else if (Input.GetAxisRaw("Horizontal") == -1)
            transform.localScale = new Vector3(-1, 1, 1);

        if (Input.GetKeyDown(KeyCode.UpArrow) && isGrounded)
        {
            rb2d.AddForce(new Vector2(0, jumpHeight));
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow) && isTramp)
        {
            rb2d.AddForce(new Vector2(0, (float)(1.5*jumpHeight)));
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundpoint.position, radius);
    }
}
